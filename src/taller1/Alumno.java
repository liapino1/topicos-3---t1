/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller1;

import javax.swing.JOptionPane;

/**
 *
 * @author luis
 */
public class Alumno {
    
    private String rut;
    private String nombre;
    private String curso;
    private double n1;
    private double n2;
    private double n3;
    private double promedio;

    /*
    public Alumno() {
        rut = "";
        nombre = "";
        curso = "";
        n1 = 0;
        n2 = 0;
        n3 = 0;

    }*/

    public Alumno(String rut, String nombre, String curso, double n1, double n2, double n3, double promedio) {
        this.rut = rut;
        this.nombre = nombre;
        this.curso = curso;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        this.promedio = promedio;
        
    }
    
    /*
    @Override
    public String toString() {
        return rut+","+nombre+","+curso+","+n1+","+n2+","+n3;
    }*/

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }

    public double getN2() {
        return n2;
    }

    public void setN2(double n2) {
        this.n2 = n2;
    }

    public double getN3() {
        return n3;
    }

    public void setN3(double n3) {
        this.n3 = n3;
    }
    
   
    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

}
